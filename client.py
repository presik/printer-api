import os
from printing import Receipt
from tools import get_config
config = get_config()

try:
    printer_name = config.get('General', 'printer_sale_name').split(',')
    profile = config.get('General', 'profile_printer')
except:
    printer_name = None
    profile = None

PATH_PRINTERS = '/dev/usb'


class Client(object):
    # Client middleware for print POS documents / receipts

    def __init__(self, context, printer=None):
        # _file = 'config_pos.ini'
        self.receipt = Receipt(
            context,
            logo=None,
            environment='restaurant'
        )
        self.config_printer(printer)

    def config_printer(self, printer=None):
        active_printers = []
        if not printer and printer is not False:
            for usb_dev in os.listdir(PATH_PRINTERS):
                if 'lp' not in usb_dev:
                    continue
                path_device = os.path.join(PATH_PRINTERS, usb_dev)
                active_printers.append(['usb', path_device])

            if active_printers:
                #  config printer automatic from list devices usb posix
                printer_sale_name = active_printers[0]
                printer = {
                    'interface': printer_sale_name[0],
                    'device': printer_sale_name[1],
                    'profile': 'TM-P80',
                }
                self.receipt.config_printer(printer)
            elif printer_name:
                #  config printer from config_pos.ini posix
                printer_ = {
                    'device': printer_name[1],
                    'interface': printer_name[0],
                    'profile': profile
                }
                self.receipt.config_printer(printer_)
        elif printer is not False:
            self.receipt.config_printer(printer)

    def test_printer(self):
        self.receipt.test_printer()

    def print_orders(self, orders):
        reversion = False
        return self.receipt.print_orders(orders.values(), reversion)

    def print_invoice(self, data):
        type_doc = 'invoice'
        self.receipt.print_sale(data, type_doc)

    def print_ticket(self):
        type_doc = 'ticket'
        self.receipt._print_ticket_system()
