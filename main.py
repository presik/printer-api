import logging
import os

from client import Client
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from starlette.middleware.cors import CORSMiddleware

DEFAULT_PATH = os.path.dirname(__file__)
DEFAULT_STATIC = DEFAULT_PATH + '/static'
log = logging.getLogger(__name__)
app = FastAPI(
    title="Printing FastAPI",
    description="Server for Printing | Presik",
    version="0.0.3",
)

origins = [
    "*",
    "http://localhost",
    "http://localhost:7011",
    "http://192.168.0.122",
    "http://192.168.0.122:7001",
    "http://cloud4.presik.com",
    "http://localhost:3100",
    "http://localhost:3000",
    # "http://cloud2.presik.com:3100/login",
]

app.debug = True
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_headers=["*"],
    allow_methods=["*"],
)

app.mount("/static", StaticFiles(directory=DEFAULT_STATIC), name="static")


@app.on_event("startup")
async def startup():
    log.info("----- Starting FastAPI server and database connection -----")


@app.on_event("shutdown")
async def shutdown():
    log.info("----- Shutdown FastAPI server -----")


@app.get("/", response_class=HTMLResponse)
def home():
    """
    Serve static index page.
    """
    with open(DEFAULT_STATIC + "/index.html", encoding='utf-8') as f:
        default_page = f.read()
    return HTMLResponse(content=default_page, status_code=200)


@app.post("/print_invoice")
def print_invoice(data: dict):
    """
    Print documents from Kid
    """
    client = Client(data['context'], printer=None)
    client.print_invoice(data['record'])
    return {"msg": "Invoice printed right!"}


@app.post("/print_order")
def print_order(data: dict):
    """
    Print orders from FastKid
    """
    client = Client(data['context'], printer=False)
    orders = data['orders']
    res = {
        "msg": 'error',
    }
    is_valid = client.print_orders(orders)
    try:
        if is_valid:
            res["msg"] = "ok"
    except Exception as e:
        res["msg"] = e
    return res


@app.post("/test_print")
def test_print(data: dict):
    """
    Print documents from Kid
    """
    client = Client(data['context'])
    client.print_printer()
    return {"msg": "All right!"}


@app.post("/test_print_ticket")
def test_print_ticket(data: dict):
    """
    Print documents from Kid
    """
    client = Client(data)
    client.print_ticket()
    return {"msg": "All right ticket!"}
