import uvicorn
from tools import get_config

config = get_config()


#with open('logging_config.json', 'rt') as f:
#    config_log = json.load(f)

cert_file = config.get('SSL', 'cert_file', fallback=None)
key_file = config.get('SSL', 'key_file', fallback=None)
host = config.get('General', 'host_printer', fallback='localhost')
port = config.get('General', 'port_printer', fallback='7001')
workers = config.get('General', 'workers', fallback='2')

if __name__ == '__main__':
    config_server = uvicorn.Config(
        'main:app', port=int(port), host=host,
        reload=True,
        workers=int(workers),
        lifespan="on"
    )
    server = uvicorn.Server(config=config_server)
    server.run()
