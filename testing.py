import requests
import simplejson as json

# The port is 7001 by default
api_url = 'http://localhost:7001'

invoice_test = {
    "context": {
        "company": "ZIRUS PIZZA SAS ",
        "sale_device": "CAJA DISPENSADOR",
        "shop": "ZIRUS DISPENSADOR",
        "user": "User App",
        "street": "CARRERA 35 51-110 LOCAL 6 CABECERA ",
        "city": "Bucaramanga",
        "phone": "6076879128",
        "id_number": "901.353.932-2",
        "regime_tax": "",
        "authorizations": {
            "P": {
                "number": "18764058951187",
                "start_date_auth": "2023-10-30",
                "end_date_auth": "2024-10-30",
                "from_auth": 1,
                "to_auth": 10000,
                "kind": "POS",
                "prefix": "ZPD"
            }
        },
        "show_position": False,
        "show_discount": True,
        "print_lines_product": None,
        "no_remove_commanded": True,
        "header": "",
        "footer": "",
        "order_copies": 1,
        "invoice_copies": 1,
        "local_printer": "http://localhost:7001"
    },
    "record": {
        "short_invoice": None,
        "party": "CLIENTE POS",
        "party_id_number": "222.222.222",
        "party_address": "Calle 67 28a 08 ",
        "party_phone": "6076879128",
        "salesman": "",
        "number": "ZPD1917",
        "order": "ZC02863",
        "date": "13/02/2024",
        "untaxed_amount": 8333.33,
        "tax_amount": 666.67,
        "total_amount": 9000,
        "discount": 0,
        "paid_amount": 9000,
        "cash_received": 0,
        "taxes": {
            "56": {
                "seq": "0",
                "name": "IMPC 8%",
                "base": 8333.3333,
                "tax": 666.666664,
                "total": 0
            }
        },
        "lines": [
            {
                "code": "POS-00514",
                "reference": None,
                "name": "COMBO PIZZA PORCION HAWAI",
                "unit_price_w_tax": 9000,
                "amount_w_tax": 9000,
                "quantity": 1,
                "taxes": [],
                "discount": None
            }
        ],
        "num_products": "1",
        "turn": None,
        "change": 0,
        "residual_amount": 0,
        "position": "",
        "create_date": "13/02/2024 10:38 AM",
        "payment_method": None,
        "comment": "",
        "pos_notes": None,
        "tip": "",
        "state": "done",
        "kind": "take_away",
        "kind_string": "Para Llevar",
        "invoice_type": "P",
        "qr_code": "",
        "cufe": None,
        "invoice_time": "10:38 AM",
        "delivery_party": "",
        "payments": [
            {
                "name": "DATAFONO",
                "amount": 9000,
                "voucher": ""
            }
        ],
        "payment_term": "CONTADO ",
        "auth_kind": "P",
        "tip_amount": 0,
        "delivery_amount": 0,
        "source": "",
        "channel": "",
        "table_assigned": "",
        "consumer": None,
        "barcode": "ZC02863"
    }
}

order_test = {
    "context": {
        "company": "ZIRUS PIZZA SAS ",
        "sale_device": "CAJA DISPENSADOR",
        "shop": "ZIRUS DISPENSADOR",
        "user": "User App",
        "street": "CARRERA 35 51-110 LOCAL 6 CABECERA ",
        "city": "Bucaramanga",
        "phone": "6076879128",
        "id_number": "901.353.932-2",
        "regime_tax": "",
        "authorizations": {
            "P": {
                "number": "18764058951187",
                "start_date_auth": "2023-10-30",
                "end_date_auth": "2024-10-30",
                "from_auth": 1,
                "to_auth": 10000,
                "kind": "POS",
                "prefix": "ZPD"
            }
        },
        "show_position": False,
        "show_discount": True,
        "print_lines_product": None,
        "no_remove_commanded": True,
        "header": "",
        "footer": "",
        "order_copies": 1,
        "invoice_copies": 1,
        "local_printer": "http://localhost:7001"
    },
    "orders": {
        "18": {
            "interface": "network",
            "host": "192.168.1.33",
            "row_characters": 48,
            "turn": "",
            "sale_number": "ZC02863",
            "number": "ZPD1917",
            "position": "",
            "party": "CLIENTE POS",
            "salesman": "",
            "consumer": {},
            "comment": "",
            "delivery_charge": None,
            "payment_term": "CONTADO ",
            "total_amount": 9000,
            "shop": "ZIRUS DISPENSADOR",
            "lines_ids": [
                1781939
            ],
            "lines": [
                {
                    "name": "COMBO PIZZA PORCION HAWAIANO +PET 250 ",
                    "quantity": "1.0",
                    "unit_price": 9000,
                    "note": None
                }
            ],
            "kind": "take_away",
            "table_assigned": "",
            "barcode": "ZC02863"
        }
    }
}


ctx = {
    'company': 'UBUNTU INC',
    'sale_device': 'CAJA-10',
    'shop': 'Shop Wall Boulevard',
    'street': 'Cll 21 # 172-81. Central Park',
    'user': 'Charles Chapplin',
    'city': 'Dallas',
    'zip': '0876',
    'phone': '591 5513 455',
    'id_number': '123456789-0',
    'tax_regime': 'NA',
    'barcode': 'Z234'
}

device = {
    'interface': 'network',
    'device': '192.168.0.10',
    'profile': 'TM-P80',
}

ticket_test = {
    'company': 'LABORATORUI BOLIVAR',
    'ticket_system': {
        'name': 'ADA WONG',
        'number_id': '666',
        'turn': 666
    }
}


def test_print_ticket():
    route = api_url + '/test_print_ticket'
    body = {
        'context': ticket_test,
        'device': device,
    }
    return route, body


def test_print():
    route = api_url + '/test_print'
    # dont pass attribute device for read config file config_pos.ini
    body = {
        'context': ctx,
        'device': device,
    }
    return route, body


def test_print_invoice():
    route = api_url + '/print_invoice'
    body = invoice_test
    return route, body


def test_print_order():
    route = api_url + '/print_order'
    body = order_test
    return route, body


if __name__ == "__main__":
    # route, body = test_print_invoice()
    route, body = test_print_ticket()

    if body:
        data = json.dumps(body)
        result = requests.post(route, data=data)
    else:
        result = requests.get(route)

    print(result.status_code, 'result')
    print(result.text, 'result')
